# URL构造形式
```
//		Objects

//	URL					HTTP Verb				Functionality
//	/object				POST					Creating Objects
//	/object/<objectId>	GET						Retrieving Objects
//	/object/<objectId>?item=xxx	PUT						Updating Objects
//	/object				GET						Queries
//	/object/<objectId>	DELETE					Deleting Objects
```
## 测试POST

- PostURL:  http://120.25.230.138:8080/object
- post数据

```
{
  "name": "zhangsan",
  "score": 90,
  "desc": [
    "学生",
    "儿童"
   ]
}
```
- 返回结果

```
{
  "name": "zhangsan",
  "score": 90,
  "desc": [
    "学生",
    "儿童"
   ]
}
```

# 主要数据结构
```
type Plan struct{
	ExecTime  string `json:"exectime"`
	Schemes  []string `json:"schemes"`
}

type User struct{
	Name string  `json:"name"`
	Plans []Plan `json:"plans"`
}

type Action struct {
	Name string `json:"name"`
	Span uint `json:"span"`
	Desc string `json:"desc"`
}
type Scheme struct{
	Name string  `json:"name"`
	Actions  []Action `json:"actions"`
	Desc string `json:"desc"`
}

type Result struct {
	Name string  `json:"name"`
	Begin  string  `json:"begin"`
	Span  uint `json:"span"`
	Score uint `json:"score"`
}
```

# API接口说明

## 用户信息

### 1. 获取用户信息
- 调用url

```
Get http://120.25.230.138:8080/user/:name/info
```
- 参数

	*  :name 用户名(zhangsan    lisi)

- 返回结果

```
{
  "name": "zhangsan",
  "chname": "张三",
  "plans": [
    {
      "exectime": "2015-07-16 15:00:00",
      "schemes": [
        "瘦身"
      ]
    },
    {
      "exectime": "2015-07-17 15:00:00",
      "schemes": [
        "健美"
      ]
    }
  ]
}
```

### 2. 创建用户
- 调用url

```
Post http://120.25.230.138:8080/user/add
```

- 传递json数据

```
{
  "name": "wangwu",
}
或者
{
  "name": "wangwu",
  "plans": [
    {
      "execTime": "2015-07-16 15:00:00",
      "schemes": [
        "jianmei"
      ]
    },
    {
      "execTime": "2015-07-17 15:00:00",
      "schemes": [
        "shoushen"
      ]
    }
  ]
}
```
- 返回结果
```
{
  "rescode": 0
}
```
rescode含义：

	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户已存在

### 3. 更新用户数据
- 调用url

```
Post http://120.25.230.138:8080/user/update
```
- 传递json数据，更新项的内容
	* 更新chname

```
{
  "name": "zhangsan",
  "chname": "张三",
 }
```
	* 更新运动计划

```
{
  "name": "zhangsan",
  "plans": [
    {
      "exectime": "2015-07-21 11:00:00",
      "schemes": [
         "jianmei"
      ]
    },
    {
      "exectime": "2015-07-18 15:00:00",
      "schemes": [
        "shoushen"
      ]
    }
  ]
}
```

- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：

	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在

### 4. 删除用户

- 调用url

```
Get http://120.25.230.138:8080/user/:name/delete
```
- 参数

	* :name  用户名(zhangsan    lisi)

- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：
	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在

## 练习结果
### 1.获取用户的练习的历史记录
- 调用url

```
Get http://120.25.230.138:8080/result/:name/all
```
- 参数

	* :name 用户名(zhangsan    lisi)

- 返回结果

```
[
  {
    "name": "zhangsan",
    "begin": "2015-07-13 14:00:00",
    "span": 600,
    "score": 7
  },
  {
    "name": "zhangsan",
    "begin": "2015-07-15 14:00:00",
    "span": 0,
    "score": 9
  },
  {
    "name": "zhangsan",
    "begin": "2015-07-21 07:00:00",
    "span": 0,
    "score": 9
  }
]
```
### 用户练习的总分数
- 调用url

```
Get Get http://120.25.230.138:8080/result/:name/totalscore
```
- 参数

	* :name 用户名(zhangsan    lisi)

- 返回结果

```
{
  "totalScore": 25
}
```

### 用户练习的总时间
- 调用url

```
Get http://120.25.230.138:8080/result/:name/totaltime
```
- 参数

	* :name 用户名(zhangsan    lisi)

- 返回结果

```
{
  "totalTime": 600
}
```

### 添加一条练习记录
- 调用url

```
Post http://120.25.230.138:8080/result/add
```
- 传递json数据，添加的内容

```
  {
    "name": "zhangsan",
    "begin": "2015-05-21 19:00:00",
    "span": 900,
    "score": 8
  }
```
- 返回结果

```
{
  "rescode": 0
}
```
rescode含义：

	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在


## 运动方案
### 1. 列出所有的运动方案名
- 调用url

```
http://120.25.230.138:8080/scheme/list
```
- 返回结果

```
{
  "Content": [
    "jianmei",
    "shoushen"
  ]
}
```

### 2. 获取指定name的运动方案
- 调用url

```
Get http://120.25.230.138:8080/scheme/:name/info
```
- 参数

	* :name 方案名(shoushen   jianmei)

- 返回结果

```
{
  "name": "shoushen",
  "chname": "瘦身",
  "actions": [
    {
      "name": "zhili",
      "span": 10,
      "desc": "直立"
    },
    {
      "name": "xiadun",
      "span": 10,
      "desc": "蹲下"
    }
  ],
  "desc": "瘦身方案"
}
```
### 3. 创建运动方案
- 调用url

```
Post http://120.25.230.138:8080/scheme/add
```

- 传递json数据

```
{
  "name": "shoushen",
  "actions": [
    {
      "name": "zhili",
      "span": 10,
      "desc": "直立"
    },
    {
      "name": "xiadun",
      "span": 10,
      "desc": "蹲下"
    }
  ],
  "desc": "瘦身方案"
}
```
- 返回结果
```
{
  "rescode": 0
}
```
rescode含义：

	* -1： 失败
	* 0 ： 成功
	* 1 ： 方案已存在

### 4. 更新方案
- 调用url

```
Post http://120.25.230.138:8080/scheme/update
```
- 传递json数据，更新项的内容

```
{
  "name": "shoushen",
  "chname": "瘦身",
  "actions": [
    {
      "name": "zhili",
      "span": 10,
      "desc": "直立"
    },
    {
      "name": "xiadun",
      "span": 10,
      "desc": "蹲下"
    }
  ],
}
```


- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：

	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在

### 5. 删除方案

- 调用url

```
Get http://120.25.230.138:8080/scheme/:name/delete
```
- 参数

	* :name  方案名(shoushen   jianmei)

- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：
	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在

### 6. 查询action

- 调用url

```
Get http://120.25.230.138:8090/scheme/:schemename/action/:actionname/info
```
- 参数

	* :schemename  方案名(shoushen   jianmei)
	* :schemename  动作名(直立   shenyao)

- 返回结果：

```
{
  "name": "zhili",
  "chname": "",
  "span": 10,
  "desc": "直立"
}
```
### 7. 新增或更新action

- 调用url

```
Post http://120.25.230.138:8090/scheme/:schemename/action/addorupdate
```
- json数据
```   
{
	"name": "wanyao",
   	"chname": "弯腰",
   "span": 600,
   "desc": "action2"
}
```

- 参数

	* :schemename  方案名(shoushen   jianmei)

- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：
	* -1： 失败
	* 0 ： 成功
	* 1 ： 用户不存在

### 8. 删除action

- 调用url

```
Get http://120.25.230.138:8090/scheme/shoushen/action/wanyao/delete
```
- 参数

	* :schemename  方案名(shoushen   jianmei)
	* :schemename  动作名(直立   shenyao)

- 返回结果：

```
{
  "rescode": 0
}
```
rescode含义：
	* -1： 失败
	* 0 ： 成功
	* 1 ： 方案或动作不存在


-------
------TOLIST------

- 关联性检查， 如练习计划包含的方案都是存在的
- 中英文名字对照
